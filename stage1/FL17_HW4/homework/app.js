function reverseNumber(num) {
	let inputNum = num;
	let reverseNum = 0;
	let str = '' + num;

	if (str[0] === '-') {
		inputNum *= -1;
	}

	while (inputNum > 0) {
		reverseNum = reverseNum * 10;
		reverseNum = reverseNum + inputNum % 10;
		inputNum = Math.floor(inputNum / 10);
	}

	if (str[0] === '-') {
		return reverseNum * -1;
	} else {
		return reverseNum;
	}
}

function forEach(arr, func) {
	for (let i of arr) {
		func(i);
	}
}

function map(arr, func) {
	let newArr = [];

	forEach(arr, function (el) {
		newArr.push(func(el));
	});

	return newArr;
}

function filter(arr, func) {
	let sortedArr = [];

	forEach(arr, function (el) {
		if (func(el)) {
			sortedArr.push(el);
		}
	});

	return sortedArr;
}

function getAdultAppleLovers(data) {
	let appleLoversArr = [];
	let nameArr = [];

	filter(data, function (el) {
		if (el.age > 18 && el.favoriteFruit === 'apple') {
			appleLoversArr.push(el);
		}
	});

	map(appleLoversArr, function (el) {
		nameArr.push(el['name']);
	});

	return nameArr;
}

function getKeys(obj) {
	let keysArr = [];

	for (let i in obj) {
		keysArr.push(i);
	}

	return keysArr;
}

function getValues(obj) {
	let valuesArr = [];

	for (let i in obj) {
		valuesArr.push(obj[i]);
	}

	return valuesArr;
}

function showFormattedDate(dateObj) {
	const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	return `It is ${dateObj.getDate()} of ${month[dateObj.getMonth()]}, ${dateObj.getFullYear()}`;
}
