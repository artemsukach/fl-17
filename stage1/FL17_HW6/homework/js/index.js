function visitLink(path) {
	let infoFromStorage = localStorage.getItem(path);

	localStorage.setItem(path, +infoFromStorage + 1);
}

function viewResults() {
	let list = document.querySelector('.result');

	if (list) {
		list.remove();
	}

	let ul = document.createElement('ul');
	let content = document.querySelector('#content');

	ul.className = 'result';
	content.append(ul);

	for (let i = 0; i < localStorage.length; i++) {
		let li = document.createElement('li');
		li.className = 'result__item';
		li.innerHTML = `You visited ${localStorage.key(i)} ${+localStorage.getItem(localStorage.key(i))} time(s)`
		ul.append(li);
	}

	localStorage.clear();
}
