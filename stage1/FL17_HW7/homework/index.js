function getAge(birthdayDate) {
	const year = 1970;
	let ageDifMs = Date.now() - birthdayDate.getTime();
	let ageDate = new Date(ageDifMs);

	return ageDate.getUTCFullYear() - year;
}

function getWeekDay(date) {
	const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

	return days[new Date(date).getDay()];
}

function getAmountDaysToNewYear() {
	const msInDay = 86400000;
	let dateNow = new Date();
	let newYear = new Date(dateNow.getFullYear() + 1, 0, 1);
	let days = newYear.getTime() - dateNow.getTime();

	return Math.round(days / msInDay);
}

function getProgrammersDay(year) {
	const msIn256Days = 22032000000;
	let newYear = new Date(year, 0, 1);
	let coderDay = new Date(newYear.getTime() + msIn256Days);

	return `${coderDay.getDate()} Sep, ${year} (${getWeekDay(coderDay)}})`;
}

function howFarIs(weekday) {
	const countOfWeekdays = 7;
	const specifiedWeekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	let today = new Date();
	let lowerCaseWeekday = weekday.toLowerCase();
	let weekdayResult = lowerCaseWeekday[0].toUpperCase() + lowerCaseWeekday.slice(1);
	let index = specifiedWeekday.indexOf(weekdayResult);

	if (today.getDay() === index) {
		return `Hey, today is ${specifiedWeekday} =)`;
	} else if (today.getDay() < index) {
		return `It's ${index - today.getDay()} day(s) left till ${weekdayResult}`;
	} else {
		return `It's ${countOfWeekdays - (today.getDay() - index)} day(s) left till ${weekdayResult}`;
	}
}

function isValidIdentifier(str) {
	let regexp = /^[A-Za-z$_]+[\w$]*$/;

	return regexp.test(str);
}

function capitalize(str) {
	return str.toLowerCase().replace(/(\s|^)[a-z]/g, el => el.toUpperCase());
}

function isValidAudioFile(str) {
	let regexp = /^[A-Za-z]+\.(mp3|flac|alac|aacs)$/;

	return regexp.test(str);
}

function getHexadecimalColors(str) {
	let regexp = /#([a-f\d]{3}){1,2}\b/gi;

	return str.match(regexp) || [];
}

function isValidPassword(str) {
	let regexp = /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}/g;

	return regexp.test(str);
}

function addThousandsSeparators(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function getAllUrlsFromText(str) {
	return str.match(/(http|https):\/\/[^\s]+/g) || [];
}