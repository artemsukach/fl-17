const appRoot = document.getElementById('app-root');
let countBtnName = 1;
let countBtnArea = 0;
const odd = 2;
const sortFalse = -1;
const sortTrue = 1;

let title = document.createElement('h1');
title.className = 'title';
title.innerHTML = 'Countries Search';

let typeOfSearch = document.createElement('p');
let searchQuery = document.createElement('p');

typeOfSearch.className = 'type-of-search__text';
searchQuery.className = 'search-query';

typeOfSearch.innerHTML = 'Please choose the type of search:';
searchQuery.innerHTML = 'Please choose search query:';

let radioButtonRegion = document.createElement('input');
radioButtonRegion.type = 'radio';
radioButtonRegion.className = 'type-of-search__radio';

let radioButtonLanguage = document.createElement('input');
radioButtonLanguage.type = 'radio';
radioButtonLanguage.className = 'type-of-search__radio';

radioButtonRegion.name = 'type-of-search__radio';
radioButtonLanguage.name = 'type-of-search__radio';

radioButtonRegion.value = 'region';
radioButtonLanguage.value = 'language';

radioButtonRegion.id = 'r1';
radioButtonLanguage.id = 'r2';
let selectQuery = document.createElement('select');
selectQuery.className = 'search-query__select';

let typeOfSearchWrapper = document.createElement('div');
typeOfSearchWrapper.className = 'field-wrapper';

let searchQueryWrapper = document.createElement('div');
searchQueryWrapper.className = 'field-wrapper';

let radioWrapper = document.createElement('div');
radioWrapper.className = 'type-of-search__radio-wrapper';

let region = document.createElement('p');
let language = document.createElement('p');

region.innerHTML = 'By Region';
language.innerHTML = 'By Language';

region.prepend(radioButtonRegion);
language.prepend(radioButtonLanguage);

radioWrapper.append(region);
radioWrapper.append(language);

typeOfSearchWrapper.prepend(typeOfSearch);
typeOfSearchWrapper.append(radioWrapper);

searchQueryWrapper.prepend(searchQuery);
searchQueryWrapper.append(selectQuery);

let selectValue = document.createElement('option');
selectValue.setAttribute('selected', 'selected');
selectQuery.setAttribute('disabled', 'disabled');
selectValue.innerHTML = 'Select value';

const regionList = externalService.getRegionsList();
const languagesList = externalService.getLanguagesList();


appRoot.append(title);
appRoot.append(typeOfSearchWrapper);
appRoot.append(searchQueryWrapper);
selectQuery.append(selectValue);

let noItemBoll = true;
let radioButtons = document.getElementsByName('type-of-search__radio');
for (let i = 0; i < radioButtons.length; i++) {
	radioButtons[i].onchange = () => {
		if (noItemBoll) {
			let noItems = document.createElement('p');
			noItems.className = 'select-value';
			noItems.innerHTML = 'No items, please choose search query';
			appRoot.append(noItems);
			selectQuery.removeAttribute('disabled');
		}
		noItemBoll = false;

		if (radioButtons[i].getAttribute('value') === 'region') {
			for (const option of document.querySelectorAll('option')) {
				option.remove();
			}
			selectQuery.append(selectValue);
			for (let el of regionList) {
				let selectValue = document.createElement('option');
				selectValue.className = 'optionItem'
				selectValue.innerHTML = `${el}`;
				selectQuery.append(selectValue);
			}
		} else {
			for (const option of document.querySelectorAll('option')) {
				option.remove();
			}
			selectQuery.append(selectValue);
			for (let el of languagesList) {
				let selectValue = document.createElement('option');
				selectValue.className = 'optionItem'
				selectValue.innerHTML = `${el}`;
				selectQuery.append(selectValue);
			}
		}
	}
}

const thArr = ['Country name', 'Capital', 'World region', 'Languages', 'Area', 'Flag'];
const objKey = ['name', 'capital', 'region', 'languages', 'area', 'flagURL']
let opt = document.querySelectorAll('optionItem');
let arrowTop = document.createElement('span');
let arrowBottom = document.createElement('span');
let arrowTopBottom = document.createElement('span');
arrowTop.innerHTML = '&#8593;';
arrowBottom.innerHTML = '&#8595;';
arrowTopBottom.innerHTML = '&#8597;';

let buttonName = document.createElement('button');
let buttonArea = document.createElement('button');

selectQuery.onchange = () => {
	createTable();
}

function createTable() {
	let isTable = document.querySelector('table');
	let isSelect = document.querySelector('.select-value');
	if (isTable) {
		isTable.remove();
	}
	if (isSelect) {
		isSelect.remove();
	}
	if (selectQuery.value === 'Select value') {
		let noItems = document.createElement('p');
		noItems.className = 'select-value';
		noItems.innerHTML = 'No items, please choose search query';
		appRoot.append(noItems);
	} else {
		let table = document.createElement('table');
		let trTh = document.createElement('tr');
		for (let el of thArr) {
			let th = document.createElement('th');
			if (el === 'Country name') {
				th.append(buttonName);
				buttonName.innerHTML = `${el}`;
				if (countBtnName === 0) {
					buttonName.append(arrowTopBottom);
				} else if (countBtnName % odd !== 0) {
					buttonName.append(arrowTop);
				} else {
					buttonName.append(arrowBottom);
				}
				trTh.append(th);
				continue;
			}
			if (el === 'Area') {
				th.append(buttonArea);
				buttonArea.innerHTML = `${el}`;
				if (countBtnArea === 0) {
					buttonArea.append(arrowTopBottom);
				} else if (countBtnArea % odd !== 0) {
					buttonArea.append(arrowTop);
				} else {
					buttonArea.append(arrowBottom);
				}
				trTh.append(th);
				continue;
			}
			th.innerHTML = `${el}`;
			trTh.append(th);
		}
		table.append(trTh);

		if (document.getElementById('r1').checked) {
			const countryListByRegion = externalService.getCountryListByRegion(selectQuery.value);
			if (countBtnName % odd === 0 && whatBtn) {
				countryListByRegion.sort((a, b) => a.name > b.name ? sortFalse : sortTrue)
			} else if (countBtnName % odd !== 0 && whatBtn) {
				countryListByRegion.sort((a, b) => a.name > b.name ? sortTrue : sortFalse)
			} else if (countBtnArea % odd === 0 && !whatBtn) {
				countryListByRegion.sort((a, b) => b.area - a.area)
			} else {
				countryListByRegion.sort((a, b) => a.area - b.area)
			}
			for (let j = 0; j < countryListByRegion.length; j++) {
				let tr = document.createElement('tr');
				for (let element of objKey) {
					let td = document.createElement('td');
					if (element === 'languages') {
						td.innerHTML = `${Object.values(countryListByRegion[j][element]).join(', ')}`;
					} else if (element === 'flagURL') {
						let img = document.createElement('img');
						img.className = 'flag';
						img.src = countryListByRegion[j][element];
						td.append(img);
					} else {
						td.innerHTML = `${countryListByRegion[j][element]}`;
					}
					tr.append(td);
				}
				table.append(tr);
			}
		}

		if (document.getElementById('r2').checked) {
			const countryListByLanguage = externalService.getCountryListByLanguage(selectQuery.value);
			if (countBtnName % odd === 0 && whatBtn) {
				countryListByLanguage.sort((a, b) => a.name > b.name ? sortFalse : sortTrue)
			} else if (countBtnName % odd !== 0 && whatBtn) {
				countryListByLanguage.sort((a, b) => a.name > b.name ? sortTrue : sortFalse)
			} else if (countBtnArea % odd === 0 && !whatBtn) {
				countryListByLanguage.sort((a, b) => b.area - a.area);
			} else {
				countryListByLanguage.sort((a, b) => a.area - b.area)
			}
			for (let j = 0; j < countryListByLanguage.length; j++) {
				let tr = document.createElement('tr');
				for (let element of objKey) {
					let td = document.createElement('td');
					if (element === 'languages') {
						td.innerHTML = `${Object.values(countryListByLanguage[j][element]).join(', ')}`;
					} else if (element === 'flagURL') {
						let img = document.createElement('img');
						img.className = 'flag';
						img.src = countryListByLanguage[j][element];
						td.append(img);
					} else {
						td.innerHTML = `${countryListByLanguage[j][element]}`;
					}
					tr.append(td);
				}
				table.append(tr);
			}
		}

		appRoot.append(table);
	}
}

let whatBtn = true;
buttonName.onclick = function () {
	countBtnArea = 0;
	whatBtn = true;
	countBtnName++;
	createTable();
};

buttonArea.onclick = function () {
	countBtnName = 0;
	whatBtn = false;
	countBtnArea++;
	createTable();
};