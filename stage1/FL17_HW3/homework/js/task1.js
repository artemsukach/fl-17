let totalAmount;
let totalProfit;

let initalAmount = +prompt('Initial amount: ');

while (!(initalAmount && initalAmount >= 1000)) {
	alert('Invalid input data');
	initalAmount = +prompt('Inital amount: ');
}

let years = +prompt('Number of years: ');

while (!(years && Number.isInteger(years) && years >= 1)) {
	alert('Invalid input data');
	years = +prompt('Number of years: ');
}

let percentage = +prompt('Percentage of year: ');

while (!(percentage && percentage >= 0 && percentage <= 100)) {
	alert('Invalid input data');
	percentage = +prompt('Percentage of year: ');
}

totalAmount = calcTotalAmount(initalAmount, years, percentage);
totalProfit = calcTotalProfit(totalAmount, initalAmount);

function calcTotalAmount(initalAmount, years, percentage) {
	let totalAmount = initalAmount;

	for (let i = 1; i <= years; i++) {
		totalAmount += totalAmount * (percentage / 100);
	}

	return totalAmount.toFixed(2);
}

function calcTotalProfit(totalAmount, initalAmount) {
	return (totalAmount - initalAmount).toFixed(2);
}

let showResult = ((totalAmount, totalProfit) => {
	alert(`Initial amount: ${initalAmount}
Number of years: ${years}
Percentage of year: ${percentage}

Total profit: ${totalProfit}
Total amount: ${totalAmount}`)
})(totalAmount, totalProfit);


