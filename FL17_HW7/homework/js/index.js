$(document).ready(function () {
	let result = 0;
	let prevEntry = 0;
	let prevOperation = null;
	let operation = null;
	let currentEntry = '0';
	let exprecions;
	let keyOfElement = 0;
	showHistory();
	updateScreen(result);
	$('.calculator__history-wrapper').scroll(function (event) {
		console.log(`Scroll Top: ${event.currentTarget.scrollTop}`);
	});

	$('.button').on('click', function () {
		let buttonPressed = $(this).html();

		if (buttonPressed === 'C') {
			result = 0;
			currentEntry = '0';
			prevEntry = 0;
			prevOperation = null;
		} else if (isNumber(buttonPressed)) {
			if (currentEntry === '0') {
				currentEntry = buttonPressed;
			} else {
				currentEntry = currentEntry + buttonPressed;
			}
		} else if (isOperator(buttonPressed)) {
			if (!prevOperation) {
				prevEntry = parseFloat(currentEntry);
			}
			operation = buttonPressed;
			prevOperation = operation;
			currentEntry = '';
		} else if (buttonPressed === '=') {
			exprecions = `${prevEntry} ${operation} ${currentEntry} = `;
			currentEntry = operate(prevEntry, currentEntry, operation);
			operation = null;
			if (currentEntry !== 'ERROR') {
				localStorage.setItem(++keyOfElement, exprecions + currentEntry);
			}
			showHistory();
		}

		updateScreen(currentEntry);
	});
});

let updateScreen = function (displayVal) {
	let displayValue = displayVal.toString();
	$('.calculator__output').html(displayValue.substring(0, 10));
};

let isNumber = function (value) {
	return !isNaN(value);
}

let isOperator = function (value) {
	return value === '/' || value === '*' || value === '+' || value === '-';
};

let operate = function (a, b, operation) {
	a = parseFloat(a);
	b = parseFloat(b);
	console.log(a, b, operation);
	if (operation === '+') {
		return a + b;
	}
	if (operation === '-') {
		return a - b;
	}
	if (operation === '*') {
		return a * b;
	}
	if (operation === '/' && b === 0) {
		return 'ERROR';
	} else {
		return a / b;
	}
}

function showHistory() {
	for (let el of $('.history__item')) {
		el.remove();
	}
	for (let key in localStorage) {
		if (!localStorage.hasOwnProperty(key)) {
			continue;
		}
		$('.calculator__history-wrapper').prepend($('<div class="history__item">' +
			`<button class="button-check" id="button-check-${key}">` +
			'</button>' +
			`<p class="history__text" id="history__text-${key}">${localStorage.getItem(key)}</p>` +
			`<button class="button-cancel" id="button-cancel-${key}">&#215;</button>` +
			'</div>'));
		$(`#button-check-${key}`).on('click', function () {
			$(this).toggleClass('button-check--red')
		});
		$(`#button-cancel-${key}`).on('click', function () {
			console.log(key);
			localStorage.removeItem(key);
			showHistory();
		});
		if (localStorage.getItem(key).includes('48')) {
			$(`#history__text-${key}`).addClass('historyText--underline');
		}
	}
}