const modifyItem = document.querySelector('#modifyItem');
const tweetItems = document.querySelector('#tweetItems');
const allertMassage = document.querySelector('#alertMessage');

const addBtn = document.querySelector('.addTweet');
const cancelBtn = document.querySelector('#cancelModification');
const saveBtn = document.querySelector('#saveModifiedItem');
let i = localStorage.length;

let likedBtn = document.createElement('button');
likedBtn.id = 'likedButton';
likedBtn.innerHTML = 'Go to liked';
likedBtn.classList.add('hidden');

let addTweetHeader = document.querySelector('#modifyItemHeader');
let inputTweet = document.querySelector('#modifyItemInput');
let listTweet = document.querySelector('#list');

let navigationButtons = document.querySelector('#navigationButtons');
navigationButtons.appendChild(likedBtn);

let backBtn = document.createElement('button');
backBtn.id = 'backButton';
backBtn.innerHTML = 'back';
backBtn.classList.add('hidden');
navigationButtons.appendChild(backBtn);

let header = document.querySelector('h1');

let alertText = document.querySelector('#alertMessageText');


// if (localStorage.length) {
// 	likedBtn.classList.remove('hidden');
// }
checkLike();
let allKeys = Object.keys(localStorage);
listCreate(allKeys);

addBtn.addEventListener('click', () => {
	history.pushState({ id: 'modifyItem' },
		null,
		location.pathname.slice(0, location.pathname.length - '11') + '#/add');
	checkLike();
	showDiv('modifyItem');
	addTweetHeader.innerHTML = 'Add tweet';
});

saveBtn.addEventListener('click', () => {
	let values = [];
	let keys = Object.keys(localStorage);
	for (let key of keys) {
		values.push(JSON.parse(localStorage.getItem(key)).value);
	}
	if (values.includes(inputTweet.value) || inputTweet.value > 140 || !inputTweet.value) {
		alertText.innerHTML = 'Error! You can`t tweet about that';
		allertMassage.classList.remove('hidden');
		setTimeout(() => allertMassage.classList.add('hidden'), 2000);
	} else {
		localStorage.setItem(`${i}`, JSON.stringify({ 'value': inputTweet.value, 'like': false }));
		history.pushState({ id: 'tweetItems' },
			null,
			location.pathname.slice(0, location.pathname.length) + '/index.html');
		checkLike();
		showDiv('tweetItems');
		let allKeys = Object.keys(localStorage);
		listCreate(allKeys);
		// likedBtn.classList.remove('hidden');
	}
	i = localStorage.length + 1;
});

cancelBtn.addEventListener('click', () => {
	history.pushState({ id: 'tweetItems' },
		null,
		location.pathname.slice(0, location.pathname.length) + '/index.html');
	checkLike();
	showDiv('tweetItems');
	let allKeys = Object.keys(localStorage);
	listCreate(allKeys);
});

likedBtn.addEventListener('click', () => {
	history.pushState({ id: 'tweetItems' },
		null,
		location.pathname.slice(0, location.pathname.length - '11') + '#/liked');
	header.innerHTML = 'Liked Tweet';
	showDiv('tweetItems');
	let allKeys = [];
	let keys = Object.keys(localStorage);
	for (let key of keys) {
		if (JSON.parse(localStorage.getItem(key)).like) {
			allKeys.push(key);
		}
	}
	listCreate(allKeys);
	addTweetHeader.innerHTML = 'Liked Tweets';
	likedBtn.classList.add('hidden');
	addBtn.classList.add('hidden');
	backBtn.classList.remove('hidden');
});

backBtn.addEventListener('click', () => {
	history.pushState({ id: 'tweetItems' },
		null,
		location.pathname.slice(0, location.pathname.length) + '/index.html');
	checkLike();
	showDiv('tweetItems');
	header.innerHTML = 'Simple Twitter';
	let allKeys = Object.keys(localStorage);
	listCreate(allKeys);
	addTweetHeader.innerHTML = 'Simple Twitter';
	if (localStorage.length) {
		// likedBtn.classList.remove('hidden');
		addBtn.classList.remove('hidden');
		backBtn.classList.add('hidden');
	}
});

function listCreate(keys) {
	while (listTweet.firstChild) {
		listTweet.removeChild(listTweet.firstChild);
	}
	for (let key of keys) {
		let itemTweet = document.createElement('li');
		itemTweet.classList.add('tweetItem');

		let itemBtn = document.createElement('p');
		itemBtn.classList.add('tweetItemBtn');
		itemBtn.addEventListener('click', () => {
			history.pushState({ id: 'tweetItems' },
				null,
				location.pathname.slice(0, location.pathname.length - '11') + `#/edit/:${key}`);
			showDiv('modifyItem');
			addTweetHeader.innerHTML = 'Edit tweet';
			inputTweet.value = JSON.parse(localStorage.getItem(key)).value;
			i = key;
		});

		let itemRemoveBtn = document.createElement('button');
		itemRemoveBtn.classList.add('tweetItemRemoveBtn');
		itemRemoveBtn.innerHTML = 'remove';
		itemRemoveBtn.addEventListener('click', () => {
			localStorage.removeItem(key)
			itemTweet.remove();
		});

		let itemLikeBtn = document.createElement('button');
		itemLikeBtn.classList.add('tweetItemLikeBtn');
		console.log(JSON.parse(localStorage.getItem(key)).like);
		if (JSON.parse(localStorage.getItem(key)).like) {
			itemLikeBtn.innerHTML = 'unlike';
		} else {
			itemLikeBtn.innerHTML = 'like';
		}
		itemLikeBtn.addEventListener('click', () => {
			localStorage.setItem(`${key}`,
				JSON.stringify({
					'value': JSON.parse(localStorage.getItem(key)).value,
					'like': !JSON.parse(localStorage.getItem(key)).like
				}));
			if (JSON.parse(localStorage.getItem(key)).like) {
				alertText.innerHTML = `Hooray! You liked tweet whit id ${key}!`;
				allertMassage.classList.remove('hidden');
				setTimeout(() => allertMassage.classList.add('hidden'), 2000);
				itemLikeBtn.innerHTML = 'unlike';
			} else {
				alertText.innerHTML = `Sorry you no longer like tweet whit id ${key}!`;
				allertMassage.classList.remove('hidden');
				setTimeout(() => allertMassage.classList.add('hidden'), 2000);
				itemLikeBtn.innerHTML = 'like';
			}
			checkLike();
		});

		itemBtn.innerHTML = JSON.parse(localStorage.getItem(key)).value;

		itemTweet.appendChild(itemBtn);
		itemTweet.appendChild(itemRemoveBtn);
		itemTweet.appendChild(itemLikeBtn);
		listTweet.appendChild(itemTweet);
	}
}

function showDiv(id) {
	const allDivs = [...document.querySelectorAll('#root > div')];
	for (let i = 0; i < allDivs.length; i++) {
		if (allDivs[i].id === id) {
			if (allDivs[i].classList.contains('hidden')) {
				allDivs[i].classList.remove('hidden');
			}
		} else {
			if (!allDivs[i].classList.contains('hidden')) {
				allDivs[i].classList.add('hidden');
			}
		}
	}
}

function checkLike() {
	let likes = [];
	let keys = Object.keys(localStorage);
	for (let key of keys) {
		likes.push(JSON.parse(localStorage.getItem(key)).like);
	}
	if (likes.includes(true)) {
		likedBtn.classList.remove('hidden');
	} else {
		likedBtn.classList.add('hidden');
	}
}

window.addEventListener('popstate', function (evt) {
	console.log()
	if (evt.state === null) {
		showDiv('tweetItems');
		header.innerHTML = 'Simple Twitter';
		// likedBtn.classList.remove('hidden');
		checkLike();
		addBtn.classList.remove('hidden');
		backBtn.classList.add('hidden');
		let allKeys = Object.keys(localStorage);
		listCreate(allKeys);
	} else {
		showDiv(evt.state.id);
	}
})
