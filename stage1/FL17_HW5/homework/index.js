let isEquals = (arg1, arg2) => arg1 === arg2;

let isBigger = (arg1, arg2) => arg1 > arg2;

let storeNames = (...str) => str;

let getDifference = (num1, num2) => num1 > num2 ? num1 - num2 : num2 - num1;

let negativeCount = (arr) => {
	let count = 0;
	for (let el of arr) {
		if (el < 0) {
			count++;
		}
	}
	return count;
};

let letterCount = (str, subStr) => {
	let count = 0;
	for (let el of str) {
		if (el === subStr) {
			count++;
		}
	}
	return count;
};

let countPoints = (arr) => {
	let points = 0;
	let splitScore;
	let winPoint = 3;
	for (let score of arr) {
		splitScore = score.split(':');
		if (+splitScore[0] > +splitScore[1]) {
			points += winPoint;
		} else if (+splitScore[0] === +splitScore[1]) {
			points++;
		}
	}
	return points;
};