function Magazine() {
	console.log('Magazine');
}
function MagazineEmployee(name, topic, magaz) {
	console.log(`${name}, ${topic}, ${magaz}`)
}
function Follower(name) {
	console.log(`${name}`)
}
const magazine = new Magazine();
const manager = new MagazineEmployee('Andrii', 'manager', magazine);
const sport = new MagazineEmployee('Serhii', 'sport', magazine);
const politics = new MagazineEmployee('Volodymyr', 'politics', magazine);
const general = new MagazineEmployee('Olha', 'general', magazine);
const iryna = new Follower('Iryna');
const maksym = new Follower('Maksym');
const mariya = new Follower('Mariya');
iryna.subscribeTo(magazine, 'sport');
maksym.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'general');
sport.addArticle('something about sport');
politics.addArticle('something about politics');
general.addArticle('some general information');
politics.addArticle('something about politics again');
sport.approve() //you do not have permissions to do it
manager.approve();//Hello Andrii. You can't approve. We don't have enough of publications
politics.publish(); //Hello Volodymyr. You can't publish. We are creating publications now.
sport.addArticle('news about sport');
manager.approve(); //Hello Andrii. You've approved the changes
sport.publish(); //Hello Serhii. You've recently published publications.
/*
something about sport Iryna
news about sport Iryna
something about politics Maksym
something about politics Mariya
something about politics again Maksym
something about politics again Mariya
some general information Mariya
*/
manager.approve('news about sport'); //Hello Andrii. While we are publishing we can't do any actions