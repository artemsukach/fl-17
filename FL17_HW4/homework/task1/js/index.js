let users;
let list = document.querySelector('.list');
let loader = document.querySelector('.loader');

async function getUsers() {
	showLoader();
	const response = await fetch('https://jsonplaceholder.typicode.com/users');
	users = await response.json();
	console.log(users);
	hideLoader();
	showUsers(users);
}

function showUsers(data) {
	data.forEach(item => {
		const card = document.createElement('li');
		card.classList.add('card');
		card.id = `${item.id}`;
		card.innerHTML = `
		<p>Name: </p>
		<p id="name${item.id}" contenteditable="true">${item.name}</p>
		<p>Username: </p>
		<p id="username${item.id}" contenteditable="true">${item.username}</p>
		<p>Email: </p>
		<p id="email${item.id}" contenteditable="true">${item.email}</p>
		<button id="save-btn-${item.id}">Save</button>
		<button id="delete-btn-${item.id}">Delete</button>`
		document.querySelector('.list').appendChild(card);
		let deleteButton = document.getElementById(`delete-btn-${item.id}`);
		let saveButton = document.getElementById(`save-btn-${item.id}`);

		deleteButton.addEventListener('click', () => {
			deleteUser(item.id, card);
		});
		saveButton.addEventListener('click', () => {
			update(item.id, data);
		});
	})
}

function deleteUser(id, item) {
	fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
		method: 'DELETE'
	})
	item.remove();
	users.splice(id, 1);
}

function update(id, listOfUsers) {
	let test = getInfo(listOfUsers);
	fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
		method: 'PUT',
		body: JSON.stringify(test),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
		}
	})
		.then((response) => response.json())
}

function showLoader() {
	list.classList.add('hide');
	loader.classList.remove('hide');
}

function hideLoader() {
	list.classList.remove('hide');
	loader.classList.add('hide');
}

function getInfo(listOfUsers) {
	let cards = document.getElementsByClassName('card');
	for (let i = 0; i < cards.length; i++) {
		listOfUsers.find((o) => {
			if (+o.id === +cards[i].id) {
				o.name = cards[i].childNodes[3].textContent;
				o.username = cards[i].childNodes[7].textContent;
				o.email = cards[i].childNodes[11].textContent;
			}
			return true;
		});
	}
	return listOfUsers;
}

getUsers();