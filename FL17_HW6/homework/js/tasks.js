function getMaxEvenElement(arr) {
	return Math.max.apply(null, arr.reduce((a, v) => v % 2 === 0 ? [...a, v] : a, []));
}

function swapVariables() {
	let a = 3;
	let b = 5;
	[a, b] = [b, a];
}

let getValue = (value) => value ?? '-';

let getObjFromArray = (arr) => Object.fromEntries(arr);

function addUniqueId(obj) {
	let pair = {
		id: Symbol()
	}

	return Object.assign({}, obj, pair);
}

let getRegroupedObject = ({
	name: firstName,
	details: { id, age, university }
}) => ({
	university,
	user: {
		age,
		firstName,
		id
	}
});

function getArrayWithUniqueElements(arr) {
	return [...new Set(arr)];
}

function hideNumber(phoneNumber) {
	return phoneNumber.replace(/(\w| )(?=(\w| ){4})/g, '*');
}

function add(a = required('a'), b = required('b')) {
	console.log(required('b'));
	return a + b;
}

function required(m) {
	throw `${m} is reqiured`;
}

function* generateIterableSequence() {
	yield 'I';
	yield 'love';
	yield 'EPAM';
}
