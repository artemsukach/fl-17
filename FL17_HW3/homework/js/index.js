'use strict';

let ingredients = [];
let prices = [];
let sum;

class Pizza {

	constructor(size, type) {
		if (arguments.length < 2) {
			throw new PizzaException(`Required two arguments, given: ${2 - arguments.length}`);
		}
		if (!Pizza.allowedSizes.includes(size.name) || !Pizza.allowedTypes.includes(type.name)) {
			throw new PizzaException('Invalid type');
		}
		this.size = size;
		this.type = type;
		prices.push(this.size.price);
		prices.push(this.type.price);
	}

	getSize() {
		return this.size;
	}

	getPrice() {
		sum = 0;
		prices.forEach(item => {
			sum += item;
		})
		return sum;
	}

	getPizzaInfo() {
		return `Size: ${this.size.name}, type: ${this.type.name}; 
extra ingredients: ${ingredients.join()}; 
price: ${this.getPrice()} UAH.`;
	}

	getExtraIngredients() {
		return ingredients;
	}

	addExtraIngredient(extraIngredient) {
		if (extraIngredient === undefined || !Pizza.allowedExtraIngredients.includes(extraIngredient.name)) {
			throw new PizzaException('Invalid ingradient');
		}
		if (extraIngredient.name
			&& Pizza.allowedExtraIngredients.includes(extraIngredient.name)
			&& !ingredients.includes(extraIngredient.name)) {
			ingredients = [...ingredients, extraIngredient.name];
			prices.push(extraIngredient.price);
		} else {
			throw new PizzaException('Duplicate ingradient');
		}
	}

	removeExtraIngredient(extraIngredient) {
		if (extraIngredient === undefined || !ingredients.includes(extraIngredient.name)) {
			throw new PizzaException('Invalid ingradient');
		}
		ingredients.splice(ingredients.indexOf(extraIngredient), 1);
		prices.splice(prices.indexOf(extraIngredient.price), 1);
	}
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = { name: 'SMALL', price: 50 };
Pizza.SIZE_M = { name: 'MEDIUM', price: 75 };
Pizza.SIZE_L = { name: 'LARGE', price: 100 };

Pizza.TYPE_VEGGIE = { name: 'VEGGIE', price: 50 };
Pizza.TYPE_MARGHERITA = { name: 'MARGHERITA', price: 60 };
Pizza.TYPE_PEPPERONI = { name: 'PEPPERONI', price: 70 };

Pizza.EXTRA_TOMATOES = { name: 'TOMATOES', price: 5 };
Pizza.EXTRA_CHEESE = { name: 'CHEESE', price: 7 };
Pizza.EXTRA_MEAT = { name: 'MEAT', price: 9 };

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S.name, Pizza.SIZE_M.name, Pizza.SIZE_L.name];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE.name, Pizza.TYPE_MARGHERITA.name, Pizza.TYPE_PEPPERONI.name];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES.name, Pizza.EXTRA_CHEESE.name, Pizza.EXTRA_MEAT.name];

class PizzaException {
	constructor(errorMessage) {
		this.log = errorMessage;
	}
}

/* It should work */
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEA); // => Invalid ingredient
