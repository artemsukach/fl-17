const fixPrize = 25;
let counter = 1;
let min = 0;
let max = 8;
let attempts = 3;
let totalPrize = 0;
let possiblePrize = 100;

let wantToPlay = confirm('Do you want to play a game?');

checkWantToPlay(wantToPlay);

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function playGame() {
	while (wantToPlay) {
		let randomNumber = getRandomInt(min, max);

		for (let i = 3; i > 0;) {
			let number = getNumber(min, max, attempts, totalPrize, possiblePrize);
			if (number === randomNumber) {
				totalPrize += possiblePrize;
				wantToPlay = congratulation(totalPrize);
				counter *= 2;
				max += 4;
				possiblePrize = 100;
				possiblePrize *= counter;
				break;
			} else {
				attempts--;
				possiblePrize -= fixPrize * counter * --i;
			}
		}

		if (attempts === 0 || !wantToPlay) {
			loss(totalPrize);
			wantToPlay = playAgain();
			possiblePrize = 100;
			totalPrize = 0;
			max = 8;
			counter = 1;
		}

		attempts = 3;

		checkWantToPlay(wantToPlay);
	}
}

function checkWantToPlay(wantToPlay) {
	if (wantToPlay === true) {
		playGame();
	} else {
		alert('You did not become a billionaire, but can.');
	}
}

function getNumber(min, max, attempts, totalPrize, possiblePrize) {
	return +prompt(`Choose a roulette pocket number from ${min} to ${max}
Attempts left: ${attempts}
Total prize: ${totalPrize}
Possible prize on current attempt: ${possiblePrize}`);
}

function congratulation(totalPrize) {
	return confirm(`Congratulation, you won! Your prize is: ${totalPrize}$. Do you want to continue?`);
}

function loss(totalPrize) {
	return alert(`Thank you for your participation. Your prize is: ${totalPrize}$.`);
}

function playAgain() {
	return confirm('Do you want to play again?');
}