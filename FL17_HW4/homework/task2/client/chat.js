let userName = prompt('Your Name');
let messageField = document.querySelector('.message-field');
let sendBtn = document.getElementById('send-btn');
let messageWrapper = document.querySelector('.messages-wrapper');
let currentUser;

let socket = new WebSocket('ws://localhost:8080');


sendBtn.addEventListener('click', () => {
	let userInfo = {
		name: userName,
		msg: messageField.value,
		time: new Date()
	};
	messageField.value = '';
	currentUser = true;
	showMessage(userInfo, currentUser);
	socket.send(JSON.stringify(userInfo));
})

socket.onmessage = (event) => {
	currentUser = false;
	showMessage(JSON.parse(event.data), currentUser);
};

function showMessage(items, current) {
	let date = new Date(Date.parse(items.time));
	let message = document.createElement('div');
	let messageContainer = document.createElement('div');
	let name = document.createElement('p');
	let text = document.createElement('p');
	let time = document.createElement('p');
	name.classList.add('message-name');
	text.classList.add('message-text');
	time.classList.add('message-time');
	name.innerHTML = items.name;
	text.innerHTML = items.msg;
	time.innerHTML = formatTime(date);
	messageContainer.append(name);
	messageContainer.append(text);
	messageContainer.append(time);
	message.classList.add('message-wrapper');
	message.append(messageContainer);
	messageContainer.classList.add('message-container');
	if (current) {
		message.classList.add('current-user');
	} else {
		message.classList.remove('current-user');
	}
	messageWrapper.prepend(message);
}

function formatTime(date) {
	let hours = date.getHours();
	let minutes = date.getMinutes();
	let seconds = date.getSeconds();
	let ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12;
	minutes = minutes < 10 ? '0' + minutes : minutes;
	let strTime = hours + ':' + minutes + ':' + seconds + ' ' + ampm;
	return strTime;
}
